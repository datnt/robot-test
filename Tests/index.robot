*** Settings ***
Documentation  This is test of help center index page
Resource  ../Resources/Common.robot
Resource  ../Resources/HelpCenter.robot
Test Setup  Start Web Test
Test Teardown  End Web Test

*** Variables ***
${HELPCENTER_URL} =  https://www.lazada.vn/helpcenter

*** Test Cases ***
Help center load index page success
    [Documentation]  This test case make sure help center page load successfully
    [Tags]  Help center  index
    Common.Page Load    ${HELPCENTER_URL}
    HelpCenter.Verify Loaded
    HelpCenter.Verify Breadcrumb
    HelpCenter.Verify Top Question
    HelpCenter.Verify Searchbox
    HelpCenter.Verify Topics


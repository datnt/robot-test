*** Settings ***
Documentation  This is the test for order status flow of chatbot
Library  SeleniumLibrary
Resource  ../Resources/Common.robot
Resource  ../Resources/HelpCenter.robot
Resource  ../Resources/PO/Chatbot.robot
Test Setup  Start Web Test
Test Teardown  End Web Test

*** Keywords ***

*** Variables ***
${HELPCENTER_URL} =  https://www.lazada.vn/helpcenter

*** Test Cases ***
User must login to see the order status
    [Documentation]  When user type order status, bot will check login status of customer, if not, bot will ask for login
    [Tags]  Smoke   Order Status
    Common.Page Load  ${HELPCENTER_URL}
    HelpCenter.Verify Loaded
    Sleep   2s
    HelpCenter.Open Chatbot
    Sleep   2s
    Chatbot.Verify Chatbot
    Chatbot.Start order status flow
    Chatbot.Verify Login Card

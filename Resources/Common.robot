*** Settings ***
Library  SeleniumLibrary
Library  RequestsLibrary


*** Variables ***
${BROWSER_APP} =  chrome



*** Keywords ***
End Web Test
    Close Browser

Start Web Test
    open browser  about:blank   ${BROWSER_APP}

Page Load
    [Arguments]  ${url}
    go to  ${url}

Verify Image Loaded
    [Arguments]  ${locator}
    page should contain image  ${locator}
    element should be visible  ${locator}
    ${img_src}=  get element attribute  ${locator}@src
    Create session  img-src  ${img_src}
    ${response}=  get  img-src  ${img_src}
    should be equal as integers  ${response.status_code}    200  image url '${img_src}' returned unexpected status code '${response.status_code}'  show_values=False
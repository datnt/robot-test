*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${BOT_WINDOW} =  id:alime-container
${INPUT_BOX} =  css:textarea.alime-input
${SEND_BUTTON} =  id:alime-send-button
${ORDER_STATUS_QUERY} =  order status
${LOGIN_BUTTON} =  css:div.alime-request-login-message

*** Keywords ***
Verify Chatbot
    Element Should Be Visible   ${BOT_WINDOW}

Start order status flow
    Input Text   ${INPUT_BOX}  ${ORDER_STATUS_QUERY}
    Sleep   1s
    Click Element   ${SEND_BUTTON}

Verify Login Card
    Wait Until Page Contains Element   ${LOGIN_BUTTON}


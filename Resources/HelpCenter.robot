*** Settings ***
Library  SeleniumLibrary
Library  RequestsLibrary
Resource  ../Resources/Common.robot

*** Variables ***
${CHABOT_BUTTON} =  css:.--js-csc-trigger

*** Keywords ***
Verify Loaded
    Wait Until Page Contains Element   ${CHABOT_BUTTON}

Open Chatbot
    Click Element    ${CHABOT_BUTTON}

Verify Breadcrumb
    page should contain element   css:#container > div > div.help-center-breadcrumb._block > div > div

Verify Searchbox
    page should contain element   css:#container > div > div.help-center-search._block.-fluid > div > div

Verify Top Question
    page should contain element   css:#container > div > div.help-center-linked-list._block > div > ul > li

Verify Topics
    ${topics_container}=   set variable  css:#container span.help-center-topic-list._icon-wrapper > img
    page should contain element  ${topics_container}
    element should be visible  ${topics_container}
    ${images}=  Get WebElements  ${topics_container}
    :FOR    ${element}  IN  @{images}
    \   ${img_src}=  set variable  ${element.get_attribute('src')}
    \   Create session  img-src  ${img_src}
    \   ${response}=  Get Request  img-src  ${img_src}
    \   should be equal as integers  ${response.status_code}    200  image url '${img_src}' returned unexpected status code '${response.status_code}'  show_values=False


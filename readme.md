# How to run

## Install dependencies
```
pip install -r /tmp/requirements.txt
```

## Run the test
```
robot -d Results Test/
robot -d Results/ -v HELPCENTER_URL:https://www.lazada.sg/helpcenter -v ORDER_STATUS_QUERY:"Where is my order" Tests/index.robot
```

# Organize test framework structure
![](https://blog.codecentric.de/wp-content/uploads/2010/07/Scalable-And-Maintainable-Acceptance-Test-Suite-600x416.png)!
# References
- [How to write a good test case](https://github.com/robotframework/HowToWriteGoodTestCases)
- [Getting started with Robot framework](https://www.slideshare.net/opendaylight/integrationgroup-robot-framework) 